# coding=utf-8
from click.testing import CliRunner

from pumari import log


def test_log():
    runner = CliRunner()
    result = runner.invoke(log, ['--user-code=CODE', '--entry-type=TYPE', '--entry-datetime=DATE'])
    assert result.exit_code == 0
    # assert result.output == 'user-code: CODE entry-type: TYPE entry-datetime: DATE\n'


if __name__ == '__main__':
    test_log()
