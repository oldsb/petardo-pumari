# coding=utf-8
from setuptools import setup

setup(
    name="Pumari",
    version='1.0',
    py_modules=['pumari'],
    install_requires=[
        'Click',
        'pytz',
    ],
    entry_points='''
        [console_scripts]
        pumari=pumari:log
    ''',

)
