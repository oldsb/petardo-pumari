# coding=utf-8
from time import sleep

import click
import pytz
from datetime import datetime

PUMARI_LOG_FILE = 'pumari.log'


@click.command()
@click.option("-u", '--user-code', required=True, help="Código de un usuario de SEGURITO.")
@click.option("-e", '--entry-type', required=True, help="Tipo de registro. [ENTRADA, SALIDA].")
@click.option("-d", '--entry-datetime', required=True, help="Fecha y hora en UTC-0.")
def log(user_code, entry_type, entry_datetime):
    datetime_now = datetime.utcnow().replace(tzinfo=pytz.utc)
    message = "current_datetime: {} user-code: {} entry-type: {} entry-datetime: {}\n".format(datetime_now,
                                                                                              user_code,
                                                                                              entry_type,
                                                                                              entry_datetime)
    sleep(15)
    click.echo(message)
    with click.open_file(PUMARI_LOG_FILE, 'a+') as f:
        f.write(message)


if __name__ == '__main__':
    log()
