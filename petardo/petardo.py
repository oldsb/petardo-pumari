import json
import os

import pika

credential_params = pika.PlainCredentials("pconpuub", "Y8IjBDtuqg2BfSnGIkRXAb0bYjEgMbax")
params = pika.ConnectionParameters(credentials=credential_params, virtual_host="pconpuub",
                                   host="lion.rmq.cloudamqp.com", heartbeat=0)
connection = pika.BlockingConnection(params)

channel = connection.channel()

channel.exchange_declare(exchange='segurito', exchange_type='fanout')
result = channel.queue_declare(queue='segurito')
queue_name = result.method.queue

channel.queue_bind(exchange='segurito', queue=queue_name)

print(" [x] Sent 'Segurito log!'")


def callback(ch, method, properties, body):
    print(" [x] Received " + str(body))
    body_dict = eval(body.decode("utf-8"))
    message = 'pumari --user-code={} --entry-type={} --entry-datetime="{}"'.format(body_dict['user-code'],
                                                                                   body_dict['entry-type'],
                                                                                   body_dict['entry-datetime'])
    print(message)
    os.system(message)


channel.basic_consume(queue=queue_name,
                      on_message_callback=callback,
                      auto_ack=True)

print(' [*] Waiting for messages:')
channel.start_consuming()
connection.close()
