FROM ubuntu:18.04
WORKDIR /opt
COPY pumari /opt
COPY petardo /opt
RUN set -xe \
    && apt-get update \
    && apt-get install python-pip -y
RUN pip install --editable .
RUN pip install -r requirements.txt
CMD python petardo.py